<?php

function Login($conn, $user, $pass)
{
    try {
        $user = htmlspecialchars($user);
        $pass = htmlspecialchars($pass);
        $pass = sha1($pass);
        $statement = $conn->prepare("SELECT username, password FROM user where username=? and password=?;");
        $statement->bindValue(1, $user, PDO::PARAM_STR);
        $statement->bindValue(2, $pass, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);;

        if (empty($result)) {
            return null;
        } else {
            return $result;
        }
    } catch (PDOException $e) {
        //echo "error:" . $e->getMessage();
        return $e;
    }
}

function DoesUserHaveToken($conn, $user)
{
    try {
        $user = htmlspecialchars($user);

        $statement = $conn->prepare("Select token from useraccess where username = ?;");
        $statement->bindValue(1, $user, PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);

        if (empty($result)) {
            return false;
        } else {
            return true;
        }
    } catch (PDOException $e) {
        return $e;
    }
}

function isValidToken($conn, $token)
{
    try {
        $user = $_COOKIE["user"];

        $statement = $conn->prepare("Select token from useraccess where token = ? and username = ?;");
        $statement->bindValue(1, $token, PDO::PARAM_STR);
        $statement->bindValue(2, $user, PDO::PARAM_STR);

        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);

        if (empty($result)) {
            return false;
        } else {
            return true;
        }

    } catch (PDOException $e) {
        return $e;
    }
}

function isUserAdmin($conn, $username)
{
    try {
        $statement = $conn->prepare("Select account-type from useraccess where username = ?;");
        $statement->bindValue(1, htmlspecialchars($username), PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);

        if (empty($result) or $result != "Admin") {
            return false;
        } else {
            return true;
        }

    } catch (PDOException $e) {
        return $e;
    }
}

function Logout($conn, $token)
{
    try {
        $statement = $conn->prepare("DELETE from useraccess where token = ?;");
        $statement->bindValue(1, $token, PDO::PARAM_STR);
        $statement->execute();

        $user = $_COOKIE["user"];
        setcookie("token", "", time() - 3600);
        setcookie("user", "", time() - 3600);

        return true;

    } catch (PDOException $e) {
        return $e;
    }
}

function renewToken($conn, $encrypted, $dateformat, $user)
{
    try {
        $statement = $conn->prepare("Update useraccess set token = ?, createdToken = ? where username = ?;");
        $statement->bindValue(1, $encrypted, PDO::PARAM_STR);
        $statement->bindValue(2, $dateformat, PDO::PARAM_STR);
        $statement->bindValue(3, $user, PDO::PARAM_STR);
        $statement->execute();
        
        //token contains the access token for one day;
        setcookie("token", $encrypted, time() + (3600 * 24), "/");
        setcookie("user", $user, time() + (3600 * 24), "/");
        return null;
    } catch (PDOException $e) {
        return $e;
    }
}

function CreateAccessToken($conn, $user)
{
    try {
        $date = new DateTime("NOW");
        $dateformat = $date->format("Y-m-d H:m:s");
        $encrypted = sha1($dateformat);
        $user = htmlspecialchars($user);

        if (DoesUserHaveToken($user) == true) {
            return renewToken($conn, $encrypted, $dateformat, $user);
        } else {
            $statement = $conn->prepare("INSERT INTO useraccess(username, token, createdToken) VALUES(?,?,?);");
            $statement->bindValue(1, $user, PDO::PARAM_STR);
            $statement->bindValue(2, $encrypted, PDO::PARAM_STR);
            $statement->bindValue(3, $dateformat, PDO::PARAM_STR);
            $statement->execute();
            
            //token contains the access token for one day;
            setcookie("token", $encrypted, time() + (3600 * 24), "/");
            setcookie("user", $user, time() + (3600 * 24), "/");

            return null;
        }
    } catch (PDOException $e) {
        return $e;
    }
}

?>