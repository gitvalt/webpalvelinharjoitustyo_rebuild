<?php

require("../../shared/log.php");

//returns logs where with $user, $type. Return null, results or error
function ReadLogs($conn, $logtype, $user)
{
    try {
        if ($logtype == "*" || $logtype == "") {
            $statement = $conn->prepare("Select * From log WHERE byUser = ? ORDER BY timestamp ASC;");
            $statement->bindValue(1, $user, PDO::PARAM_STR);

        } else {
            $statement = $conn->prepare("Select * From log WHERE byUser = ? AND type = ? ORDER BY timestamp ASC;");
            $statement->bindValue(1, $user, PDO::PARAM_STR);
            $statement->bindValue(2, $logtype, PDO::PARAM_STR);

        }

        $statement->execute();
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        if (empty($results)) {
            return null;
        } else {
            return $results;
        }

    } catch (PDOException $e) {
        return $e;
    }
}

//returns logs where with $user, $type. Return true or error
function CreateNewLog($conn, Log $log, $user)
{
    try {
        $conn = Connect();
        $statement = $conn->prepare("INSERT INTO log(type, description, targetUser) VALUE(?, ?, ?);");
        $statement->bindValue(1, $log->Type, PDO::PARAM_STR);
        $statement->bindValue(2, $log->Description, PDO::PARAM_STR);
        $statement->bindValue(3, $log->ByUser, PDO::PARAM_STR);
        $statement->execute();
        return null;

    } catch (PDOException $e) {
        return $e;
    }
}
?>