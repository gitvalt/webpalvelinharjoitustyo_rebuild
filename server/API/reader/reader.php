<?php

require("logs.php");
require("user_managment.php");

class SqlConnector
{
    private $connection;
    private $SQLhost, $dbName, $dbUser, $dbPass;

    //Connector constructed --> establish connection
    function __construct($host, $db_name, $user, $pass)
    {
        if (empty($host) || empty($db_name)) {
            throw new Exception("SqlConnector.Constructor: host or database undefined");
        }

        $connection = createConnection($host, $dbName, $user, $pass);

        if (typeof($connection) == PDOException) {
            throw new Exception("SqlConnector.Constructor: Could not create connection to server");
        }

        $SQLhost = $host;
        $dbName = $db_name;
        $dbUser = $user;
        $dbPass = $pass;
    }

    //on destruct, close existing connection
    function __destruct()
    {
        breakConnection();
    }



    //---Connection managment---

    //check if connection exists
    function isConnectionClosed()
    {
        if ($connection == null) {
            return true;
        } else {
            return false;
        }
    }

    function ensureConnection()
    {
        if (isConnectionClosed) {
            try {
                $connection = createConnection($SQLhost, $dbName, $dbUser, $dbPass);
                return null;
            } catch (PDOException $err) {
                return $err;
            }
        }
    }

    function createConnection($host, $dbName, $user, $pass)
    {
        try {
            $conn = new PDO("mysql:host=$host;dbname=$database", $user, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conn->exec("SET CHARACTER SET utf8");
            return $conn;
        } catch (PDOExecption $ex) {
            echo $ex->getMessage();
            return $ex;
        }
    }

    function breakConnection()
    {
        $connection = null;
    }

    

    //---Log methods---

    function Get_Log($type, $username)
    {
        $err = ensureConnection();
        if (!empty($err)) {
            return $err;
        }
        return ReadLogs($connection, $type, $username);
    }

    function Create_Log(Log $log, $user)
    {
        $err = ensureConnection();
        if (!empty($err)) {
            return $err;
        }
        return CreateNewLog($connection, $log, $user);
    }

    //---User---

    function Create_User(User $user, $pwd)
    {
        $err = ensureConnection();
        if (!empty($err)) {
            return $err;
        }
        return CreateNewUser($connection, $user, $pwd);
    }

    function Delete_User($username)
    {
        $err = ensureConnection();
        if (!empty($err)) {
            return $err;
        }
        return DeleteUser($connection, $username);
    }

    function Get_User($username)
    {
        $err = ensureConnection();
        if (!empty($err)) {
            return $err;
        }
        return GetUser($connection, $username);
    }

    //---Login---
}



?>