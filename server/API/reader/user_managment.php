<?php

require("../../shared/user.php");

//Create user
function CreateNewUser($conn, User $user, $password)
{
    if ($password == null) {
        throw new Exception("CreatingNewUser: password is empty");
    }
    try {
        $conn = Connect();

        $statement = $conn->prepare("INSERT INTO user(username, password, firstname, lastname, email, phone, address) VALUES(?,?,?,?,?,?,?);");

        $statement->bindValue(1, $user->Username, PDO::PARAM_STR);
        $statement->bindValue(2, $password, PDO::PARAM_STR);
        $statement->bindValue(3, $user->Firstname, PDO::PARAM_STR);
        $statement->bindValue(4, $user->Lastname, PDO::PARAM_STR);
        $statement->bindValue(5, $user->Email, PDO::PARAM_STR);
        $statement->bindValue(6, $user->Phone, PDO::PARAM_STR);
        $statement->bindValue(7, $user->Address, PDO::PARAM_STR);

        $statement->execute();
        return null;
    } catch (PDOException $e) {
        return $e->getMessage();
    }
}

function DeleteUser($conn, $username)
{
    try {
        $statement = $conn->prepare("DELETE FROM user WHERE username=? ;");
        $statement->bindValue(1, $username, PDO::PARAM_STR);
        $statement->execute();
        return null;

    } catch (PDOException $e) {
        return $e->getMessage();
    }
}

//Get user profile
function GetUser($conn, $username)
{
    try {
        $statement = $conn->prepare("SELECT username, firstname, lastname, phone, email, address FROM user where username=?;");
        $statement->bindValue(1, htmlspecialchars($user), PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);

        if (empty($result)) {
            return null;
        } else {
            return $result;
        }

    } catch (PDOException $e) {
        return $e;
    }
}

function GetUserWithEmail($conn, $email)
{
    try {
        $statement = $conn->prepare("SELECT username, firstname, lastname, phone, email, address FROM user where email=?;");
        $statement->bindValue(1, htmlspecialchars($email), PDO::PARAM_STR);
        $statement->execute();
        $result = $statement->fetch(PDO::FETCH_ASSOC);

        if (empty($result)) {
            return null;
        } else {
            return $result;
        }

    } catch (PDOException $e) {
        return $e;
    }
}


function DoesUserExist($conn, $username, $email)
{
    if (empty($username) && empty($email)) {
        throw new Exception("DoesUserExist: username and email are empty. Define at least one.");
    }

    if (!empty($username)) {
        if (GetUser($username) == null) {
            return false;
        }
        return true;
    }

    if (!empty($email)) {
        if (GetUserWithEmail($email) == null) {
            return false;
        }
        return true;
    }
}

function ModifyUser($conn, User $user, $password)
{
    if(DoesUserExist($user->Username, null)){
        return new Exception("ModifyUser: Could not find user");
    }

    try {
        $conn = Connect();
        $statement = $conn->prepare("UPDATE user SET password=?, firstname=?, lastname=?, email=?, phone=?, address=? WHERE username=?;");

        $statement->bindValue(1, $password, PDO::PARAM_STR);
        $statement->bindValue(2, $user->Firstname, PDO::PARAM_STR);
        $statement->bindValue(3, $user->Lastname, PDO::PARAM_STR);
        $statement->bindValue(5, $user->Phone, PDO::PARAM_STR);
        $statement->bindValue(6, $user->Address, PDO::PARAM_STR);
        $statement->bindValue(4, $user->Email, PDO::PARAM_STR);
        $statement->bindValue(7, $user->Username, PDO::PARAM_STR);

        $statement->execute();
        CreateLog("Modify user", "$user->Username was modified", $user->Username);
        return null;

    } catch (PDOException $e) {
        //echo "error:" . $e->getMessage();
        return $e;
    }
}

?>