<?php

class User {
    public $Username; 
    public $Firstname, $Lastname; 
    public $Email; 
    public $Phone; 
    public $Address;

    function __construct($user, $first, $last, $email, $phone, $address)
    {
        if(empty($user) || empty($email)) { throw new Exception("User.construct: username or email are null"); }
        $Username = $user;
        $Firstname = $first;
        $Lastname = $last;
        $Email = $email;  
        $Phone = $phone;
        $Address = $address;
    }

    function getFullname(){ return $Firstname + " " + $Lastname; }
}

?>